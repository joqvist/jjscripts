#!/bin/bash

set -e

if [ $# -lt "1" ]; then
    echo "Usage: $0 <Class Name> [Tag]"
	echo "<...> required"
	echo "[...] optional"
    exit 1
fi

echo "Checking $1"

SRC=test/$1.java

if [ ! -f "${SRC}" ]; then
	echo "Error: ${SRC} does not exist!"
	exit 1
fi

if [ ! -z "$2" ]; then
	N=$1.$2
else
	N=$1
fi

ASM=../ow2-asm
ASM_CP="${ASM}/output/build/tmp"

# build ASM if needed
if [ ! -f "${ASM_CP}/org/objectweb/asm/util/CheckClassAdapter.class" ]; then
	ant -f ${ASM}/build.xml compile
fi

EXTENDJ="${EXTENDJ:-extendj.jar}"
OUTDIR="tmp"

if [ ! -d "${OUTDIR}" ]; then
	mkdir "${OUTDIR}"
fi

if [ ! -z "$JAVAC" ]; then
	javac -Xlint:unchecked -g -d "${OUTDIR}" ${SRC} 2>&1 | tee "${OUTDIR}"/$N.log
else
	java -Xss8M -jar $EXTENDJ -d "${OUTDIR}" ${SRC} 2>&1 | tee "${OUTDIR}"/$N.log
fi

javap -verbose -c -p "${OUTDIR}"/${1}.class > "${OUTDIR}"/$N.bytecode

if [ ! -z "$VERIFY" ]; then
	java -cp "${ASM_CP}" org.objectweb.asm.util.CheckClassAdapter "${OUTDIR}/${1}.class" &> "${OUTDIR}/$N.verifier"
else
	java -cp "${ASM_CP}" org.objectweb.asm.util.Textifier "${OUTDIR}/${1}.class" &> "${OUTDIR}/$N.text"
fi

if [ -z "$NOEXEC" ]; then
	java -cp "${OUTDIR}" "${1}"
fi
